exports.errors = [
  {
    message: 'Request error',
    status: 400
  },
  {
    message: 'User has already exist',
    status: 400
  },
  {
    message: 'Fighter has already exist',
    status: 400
  },
  {
    message: 'Entity to create isn\'t valid',
    status: 400
  },
  {
    message: 'Data isn\'t valid for entity update',
    status: 400
  },
  {
    message: 'User not found',
    status: 404
  },
  {
    message: 'Fighter not found',
    status: 404
  }
]