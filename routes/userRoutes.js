const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    res.data = UserService.getAllUsers();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
  try {
    const userId = req.params.id;
    res.data = UserService.getUser(userId);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, function(req, res, next) {
  try {
    res.data = UserService.createUser(req.body)
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, function(req, res, next) {
  try {
    const userId = req.params.id;
    const dataToUpdate = req.body;
    res.data = UserService.updateUser(userId, dataToUpdate);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
  const userId = req.params.id;
  UserService.deleteUser(userId);
});

module.exports = router;