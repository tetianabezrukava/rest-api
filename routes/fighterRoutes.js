const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    res.data = FighterService.getAllFighters();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
  try {
    const fighterId = req.params.id;
    res.data = FighterService.getFighter(fighterId);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, function(req, res, next) {
  try {
    res.data = FighterService.createFighter(req.body)
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, function(req, res, next) {
  try {
    const fighterId = req.params.id;
    const dataToUpdate = req.body;
    res.data = FighterService.updateFighter(fighterId, dataToUpdate);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
  const fighterId = req.params.id;
  FighterService.deleteFighter(fighterId);
});

module.exports = router;