const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  search(search) {
    const item = FighterRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
  
  getFighter(fighterId) {
    const fighter = this.search(it => it.id === fighterId);
    if(!fighter) {
      throw Error('Fighter not found');
    }
    return fighter;
  }
  
  getAllFighters() {
    const fighters = FighterRepository.getAll();
    if(!fighters) {
      throw Error('Request error');
    }
    return fighters;
  }
  
  createFighter(data) {
    const fighterAlreadyExist = this.search(it => it.name === data.name);
    if (fighterAlreadyExist) {
      throw Error('Fighter has already exist')
    }
    return  FighterRepository.create(data);
  }
  
  updateFighter(id, dataToUpdate) {
    if(Object.keys(dataToUpdate).includes('name')) {
      if (this.search(it => it.name === dataToUpdate.name)) {
        throw Error('User has already exist')
      }
    }
    
    return FighterRepository.update(id, dataToUpdate)
  }
  
  deleteFighter(id) {
    return FighterRepository.delete(id)
  }
}

module.exports = new FighterService();