const { UserRepository } = require('../repositories/userRepository');

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getUser(userId) {
        const user = this.search(it => it.id === userId);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
    
    getAllUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            throw Error('Request error');
        }
        return users;
    }
    
    createUser(data) {
        const emailAlreadyExist = this.search(it => it.email === data.email);
        const phoneNumberAlreadyExist = this.search(it => it.phoneNumber === data.phoneNumber);
        const userAlreadyExist = emailAlreadyExist || phoneNumberAlreadyExist;
        if (userAlreadyExist) {
            throw Error('User has already exist')
        }
        return  UserRepository.create(data);
    }
    
    updateUser(id, dataToUpdate) {
        if(Object.keys(dataToUpdate).includes('email')) {
            if (this.search(it => it.email === dataToUpdate.email)) {
                throw Error('User has already exist')
            }
        }
    
        if(Object.keys(dataToUpdate).includes('phoneNumber')) {
            if (this.search(it => it.phoneNumber === dataToUpdate.phoneNumber)) {
                throw Error('User has already exist')
            }
        }
        
        return UserRepository.update(id, dataToUpdate)
    }
    
    deleteUser(id) {
        return UserRepository.delete(id)
    }
}

module.exports = new UserService();