const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    if(!req.body.health) {
        req.body.health = 100;
    }
    
    const { name, health, power, defense } = req.body;
    
    const modelFighterKeys = Object.keys(fighter).filter(it => it !== 'id' && it !== 'health' ).sort();
    const newFighterKeys = Object.keys(req.body).filter(it => it !== 'health' ).sort();
    const hasRequiredFields = JSON.stringify(modelFighterKeys) === JSON.stringify(newFighterKeys);
    
    if(!hasRequiredFields) {
        res.status(400)
        res.json({error: true, message: 'Entity to create isn\'t valid'})
        res.send()
    } else {
        const hasValidName = !!name.trim().length;
        const hasValidPower = !isNaN(+power) && +power >= 1 && +power <= 100;
        const hasValidDefense = !isNaN(+defense) && +defense >= 1 && +defense <= 10;
        const hasValidHealth = !isNaN(+health) && +health >= 80 && +health <= 120;
    
        if(!(
          hasValidName &&
          hasValidPower &&
          hasValidDefense &&
          hasValidHealth)) {
            res.status(400)
            res.json({error: true, message: 'Entity to create isn\'t valid'})
            res.send()
        } else {
            next();
        }
    }
}

const updateFighterValid = (req, res, next) => {
    const modelFighterKeys = Object.keys(fighter).filter(it => it !== 'id');
    const updateFighterKeys = Object.keys(req.body);
    let hasValidFields = true;
    
    for (let key of updateFighterKeys) {
        if (!modelFighterKeys.includes(key)) {
            hasValidFields = false;
        }
    }
    
    if (!(
      updateFighterKeys.length &&
      hasValidFields &&
      updateFighterKeys.every(fieldIsValid)
    )) {
        res.status(400)
        res.json({error: true, message: `Data isn\'t valid for entity update`})
        res.send()
    } else {
        next();
    }
    
    function fieldIsValid(field) {
        const { body } = req;
        let isValid = true;
        if (field === 'name') {
            isValid = body.name.trim().length ? isValid : false;
        }
    
        if (field === 'health') {
            isValid = !isNaN(body.health) && body.health >= 80 && body.health <= 120 ? isValid : false;
        }
    
        if (field === 'power') {
            isValid = !isNaN(body.power) && body.power >= 1 && body.power <= 100 ? isValid : false;
        }
    
        if (field === 'defense') {
            isValid = !isNaN(body.defense) && body.defense >= 1 && body.defense <= 10 ? isValid : false;
        }
        
        return isValid;
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;