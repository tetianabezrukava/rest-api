const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    
    const modelUserKeys = Object.keys(user).filter(it => it !== 'id').sort();
    const newUserKeys = Object.keys(req.body).sort();
    const hasRequiredFields = JSON.stringify(modelUserKeys) === JSON.stringify(newUserKeys);
    
    if(!hasRequiredFields) {
        res.status(400)
        res.json({error: true, message: 'Entity to create isn\'t valid'})
        res.send()
    } else {
        const hasValidEmail = email.toLocaleLowerCase().endsWith('@gmail.com');
        const hasValidFirstName = !!firstName.trim().length;
        const hasValidLastName = !!lastName.trim().length;
        const hasValidPassword = password.length >= 3;
        const hasValidPhoneNumber = /\+380\d{9}/.test(phoneNumber);
    
        if(!(
          hasRequiredFields &&
          hasValidFirstName &&
          hasValidLastName &&
          hasValidEmail &&
          hasValidPhoneNumber &&
          hasValidPassword)) {
            res.status(400)
            res.json({error: true, message: 'Entity to create isn\'t valid'})
            res.send()
        } else {
            next();
        }
    }
}

const updateUserValid = (req, res, next) => {
    const modelUserKeys = Object.keys(user).filter(it => it !== 'id');
    const updateUserKeys = Object.keys(req.body);
    let hasValidFields = true;
    
    for (let key of updateUserKeys) {
        if (!modelUserKeys.includes(key)) {
            hasValidFields = false;
        }
    }
    
    if (!(
      updateUserKeys.length &&
      hasValidFields &&
      updateUserKeys.every(fieldIsValid)
    )) {
        res.status(400)
        res.json({error: true, message: `Data isn\'t valid for entity update`})
        res.send()
    } else {
        next();
    }
    
    function fieldIsValid(field) {
        const { body } = req;
        let isValid = true;
        if (field === 'firstName') {
            isValid = body.firstName.trim().length ? isValid : false;
        }
    
        if (field === 'lastName') {
            isValid = body.lastName.trim().length ? isValid : false;
        }
        
        if (field === 'password') {
            isValid = body.password.length >= 3 ? isValid : false;
        }
        
        if (field === 'email') {
            isValid = body.email.toLocaleLowerCase().endsWith('@gmail.com') ? isValid : false;
        }
    
        if (field === 'phoneNumber') {
            isValid = /\+380\d{9}/.test(body.phoneNumber) ? isValid : false;
        }
        
        return isValid;
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;